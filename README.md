# README

    sudo docker-compose up # Run app  
    sudo docker-compose up --build # Build container and service

[Source](https://reinteractive.com/posts/359-docker-for-rails-development)

Can update files with after the app is booted, but cannot generate new commands
and update the application until it is stopped and the bootsnap cache is
cleared. `tmp/cache/bootsnap-compile-cache`

This README would normally document whatever steps are necessary to get the
application up and running.

Things you may want to cover:

* Ruby version

* System dependencies

* Configuration

* Database creation

* Database initialization

* How to run the test suite

* Services (job queues, cache servers, search engines, etc.)

* Deployment instructions

* ...
